/**
 * a processable asset type defines a blueprint for a processable assets
 */
export interface IProcessableAssetType {
  typeName: string;
  typeIconUrl: string;
  typeBufferTimes: {
    before: number;
    after: number;
  };
  
}