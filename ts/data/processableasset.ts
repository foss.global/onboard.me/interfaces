/**
 * a trackable asset can be a car, truck, boat, plane, etc.
 */
export interface IProcessableAsset {
  type: string;
  id: string;
  name: string;

}