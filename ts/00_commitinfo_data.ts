/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@onboard.me/interfaces',
  version: '1.0.3',
  description: 'the interface package for onboard.me application'
}
