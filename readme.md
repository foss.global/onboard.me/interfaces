# @onboard/interfaces
the interface package for onboard.me application

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@onboard.me/interfaces)
* [gitlab.com (source)](https://gitlab.com/onboard/interfaces)
* [github.com (source mirror)](https://github.com/onboard/interfaces)
* [docs (typedoc)](https://onboard.gitlab.io/interfaces/)

## Status for master

Status Category | Status Badge
-- | --
GitLab Pipelines | [![pipeline status](https://gitlab.com/onboard/interfaces/badges/master/pipeline.svg)](https://lossless.cloud)
GitLab Pipline Test Coverage | [![coverage report](https://gitlab.com/onboard/interfaces/badges/master/coverage.svg)](https://lossless.cloud)
npm | [![npm downloads per month](https://badgen.net/npm/dy/@onboard.me/interfaces)](https://lossless.cloud)
Snyk | [![Known Vulnerabilities](https://badgen.net/snyk/onboard/interfaces)](https://lossless.cloud)
TypeScript Support | [![TypeScript](https://badgen.net/badge/TypeScript/>=%203.x/blue?icon=typescript)](https://lossless.cloud)
node Support | [![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
Code Style | [![Code Style](https://badgen.net/badge/style/prettier/purple)](https://lossless.cloud)
PackagePhobia (total standalone install weight) | [![PackagePhobia](https://badgen.net/packagephobia/install/@onboard.me/interfaces)](https://lossless.cloud)
PackagePhobia (package size on registry) | [![PackagePhobia](https://badgen.net/packagephobia/publish/@onboard.me/interfaces)](https://lossless.cloud)
BundlePhobia (total size when bundled) | [![BundlePhobia](https://badgen.net/bundlephobia/minzip/@onboard.me/interfaces)](https://lossless.cloud)

## Usage

Use TypeScript for best in class intellisense
For further information read the linked docs at the top of this readme.

## Legal
> UNLICENSED licensed | **&copy;** [Task Venture Capital GmbH](https://task.vc)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)
